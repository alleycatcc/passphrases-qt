#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0"

rootdir="$bindir"/..
frontenddir="$rootdir"/passphrases-frontend
resourcesdirbase="$rootdir"/resources
resourcesdirstub=frontend
resourcesdirfull="$resourcesdirbase"/"$resourcesdirstub"

fun safe-rm-dir-allow-absolute "$resourcesdirbase" "$resourcesdirstub"
mkd "$resourcesdirfull"

# --- legacy
legacy__files=(
    css
    fonts
    images
    index.html
    js/index.js
    js/data/wordlists/catalan.js
    js/data/wordlists/dutch.js
    js/data/wordlists/english.js
    js/data/wordlists/french.js
    js/data/wordlists/german.js
    js/data/wordlists/italian.js
    js/data/wordlists/japanese.js
    js/data/wordlists/polish.js
    js/data/wordlists/securedrop.js
    js/data/wordlists/swedish.js
)

dir=build

go-legacy () {
    local full
    local file

    for file in "${files[@]}"; do
        full="$resourcesdirfull"/"$file"
        base=$(dirname "$full")
        if [ ! -d "$base" ]; then
            mkd "$base"
        fi
        cpa "$frontenddir"/"$file" "$base"
    done
}

go () {
    cpa "$frontenddir"/build/* "$resourcesdirfull"
}

fun go
