#ifndef CAT_GEL_ROOT_H
#define CAT_GEL_ROOT_H

#include <vector>

#ifdef PLATFORM_ANDROID
# include <android/log.h>
# include <jni.h>
# include <QtAndroidExtras/QAndroidJniEnvironment>
# include <QtAndroidExtras/QAndroidJniObject>
#endif

#include <QDebug>
#include <QQmlApplicationEngine>
#include <QVariantMap>

#include "catqt_global.h"
#include "catqt_qml.h"

// --- use strstr to double quote a macro.
#define str(s) #s
#define strstr(s) str(s)

#define JNI_CLASS cc/alleycat/android/passphrases/JNIActivity

#ifdef VARIANT_TEST
# define DEBUG_FRONTEND 1
#else
# define DEBUG_FRONTEND 0
#endif

using std::vector;

class CatPpRoot : public QObject
{
    Q_OBJECT

public:
    explicit CatPpRoot(QObject *parent = nullptr);

    static CatPpRoot *instance;
    void setQmlApplicationEngine(QQmlApplicationEngine *);

// --- these should be slots in order to be called from qml.
public slots:
    QVariantMap initActivity(QString urlBase);
    bool pageLoaded();

private:
    QQmlApplicationEngine *engine;
#ifdef PLATFORM_ANDROID
    // bool initActivityAndroid();
    QVariantMap initActivityAndroid();
    bool pageLoadedAndroid();
#endif

protected:
};

#endif
