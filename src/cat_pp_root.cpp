#include <iostream>

#include "cat_pp_root.h"

// --- level 5 is warn.
void cat_debug(QString s)
{
    qDebug() << s;
#ifdef PLATFORM_ANDROID
    __android_log_write(5, "cat-pp-root", s.toLatin1().data());
#endif
}


CatPpRoot *CatPpRoot::instance = nullptr;

CatPpRoot::CatPpRoot(QObject *parent) : QObject(parent)
{
    instance = this;
}

void CatPpRoot::setQmlApplicationEngine(QQmlApplicationEngine *engine)
{
    this->engine = engine;
}

QVariantMap CatPpRoot::initActivity(QString urlBase)
{
    QString passphrase = nullptr;
    int roundIdx = -1;
    int numWords = -1;
    QString selectedList = nullptr;
    bool hasSaved;

#ifdef PLATFORM_ANDROID
    QVariantMap init = initActivityAndroid();

    bool ok = init["ok"].toBool();
    hasSaved = init["hasSaved"].toBool();

    if (ok) {
        if (!hasSaved) cat_debug(QString("Init ok but nothing saved"));
        else {
            passphrase = init["passphrase"].toString();
            roundIdx = init["roundIdx"].toInt();
            numWords = init["numWords"].toInt();
            selectedList = init["selectedList"].toString();
            if (passphrase == nullptr) cat_debug(QString("Got null passphrase"));
            else cat_debug(QString("Got passphrase: %1").arg(passphrase));
            if (roundIdx == -1) cat_debug(QString("Got no roundIdx"));
            else cat_debug(QString("Got roundIdx: %1").arg(roundIdx));
            if (numWords == -1) cat_debug(QString("Got no numWords"));
            else cat_debug(QString("Got numWords: %1").arg(numWords));
            if (selectedList == nullptr) cat_debug(QString("Got no selectedList"));
            else cat_debug(QString("Got selectedList: %1").arg(selectedList));
        }
    }
#else
    bool ok = true;
    // passphrase = "[mock] welke zweeg fors zonden pel";
    std::cout << "[mock] initActivity" << std::endl;
    hasSaved = false;

#endif
    QString urlEnd;
    if (hasSaved) urlEnd = QString(
        "&passphrase=%1&roundIdx=%2&numWords=%3&selectedList=%4#memorize"
    ).arg(passphrase).arg(roundIdx).arg(numWords).arg(selectedList);
    else urlEnd = QString("");
    QString url = QString("%1?debug=%2%3").
        arg(urlBase).arg(DEBUG_FRONTEND).arg(urlEnd);
    QVariantMap ret;
    ret.insert("ok", ok);
    ret.insert("url", url);
    return ret;
}

bool CatPpRoot::pageLoaded()
{
#ifdef PLATFORM_ANDROID
    return pageLoadedAndroid();
#else
    std::cout << "[mock] pageLoaded" << std::endl;
    return true;
#endif
}

#if 0
bool CatPpRoot::initActivityAndroidOld()
{
    const char *signature = "()Z";
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        strstr(JNI_CLASS),
        "init",
        signature
    );
    return (bool)ok;
}
#endif

#ifdef PLATFORM_ANDROID
QVariantMap CatPpRoot::initActivityAndroid()
{
    const char *signature = "()Lcc/alleycat/android/passphrases/InitDTO;";
    QAndroidJniObject init = QAndroidJniObject::callStaticObjectMethod(
        strstr(JNI_CLASS),
        "init",
        signature
    );
    jboolean ok = init.getField<jboolean>("ok");
    jboolean hasSaved = init.getField<jboolean>("hasSaved");
    QAndroidJniObject passphraseJ = init.getObjectField("passphrase", "Ljava/lang/String;");
    QString passphrase = passphraseJ.toString();
    jint roundIdx = init.getField<jint>("roundIdx");

    QVariantMap ret;
    ret.insert("ok", (bool)ok);
    ret.insert("hasSaved", (bool)hasSaved);
    ret.insert("passphrase", passphrase);
    ret.insert("roundIdx", (int)roundIdx);
    return ret;
}
#endif

#ifdef PLATFORM_ANDROID
bool CatPpRoot::pageLoadedAndroid()
{
    const char *signature = "()Z";
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        strstr(JNI_CLASS),
        "pageLoaded",
        signature
    );
    return (bool)ok;
}
#endif
