package cc.alleycat.android.passphrases;

public interface InitDTO {
    boolean ok = false;
    boolean hasSaved = false;
    String passphrase = null;
    int roundIdx = -1;
    int numWords = -1;
    String selectedList = null;
}
