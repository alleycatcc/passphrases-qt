package cc.alleycat.android.passphrases;

import android.R.id;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;

// --- require special repos pulled in through maven.
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;

import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.QtLayout;
import org.qtproject.qt5.android.QtSurface;

// xxx
import org.qtproject.qt5.android.QtActivityDelegate;

import cc.alleycat.android.qtwebview.QtWebViewCommon;
import cc.alleycat.android.passphrases.InitDTO;

public class JNIActivity extends QtActivity {
    private static final String TAG = "Passphrases.JNIActivity";
    private static final String HOST_CHANNEL_FROM_JS = "hostChannelFromJS";
    // private static final String NOTIFY_TITLE = "Passphrases";

    private static final String CHANNEL_ID = "blah";
    private static final String PREF_KEY_PASSPHRASE = "passphrase";
    private static final String PREF_KEY_ROUND_IDX = "round-idx";
    private static final String PREF_KEY_NUM_WORDS = "num-words";
    private static final String PREF_KEY_SELECTED_LIST = "selected-list";

    private static JNIActivity instance;

    private static boolean resumed = false;
    private static boolean initted = false;

    private static WebView webview;

    private static NotificationCompat.Builder builder;

    private static Configuration configuration = null;

    public JNIActivity() {
        instance = this;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (configuration == null) {
            Log.w(TAG, String.format("onConfigurationChanged(): %s, no cur conf, can't diff", newConfig.toString()));
        }
        else {
            int diff = newConfig.diff(configuration);
            Log.w(TAG, String.format("onConfigurationChanged(): %s, diff = %d", newConfig.toString(), diff));
        }
        configuration = newConfig;
        super.onConfigurationChanged(newConfig);
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        boolean hasBundle = savedInstanceState != null;
        Log.w(TAG, String.format("onCreate(): bundle exists: %b", hasBundle));
    }

    // --- part of the launch steps, and also when user returns.
    @Override
    protected void onResume() {
        Log.w(TAG, String.format("onResume ()"));
        resumed = true;
        super.onResume();
    }

    // --- called on pause and always before onStop().
    @Override
    protected void onPause() {
        resumed = false;
        Log.w(TAG, String.format("onPause ()"));
        super.onPause();
    }

    // --- not called when user closes on purpose.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.w(TAG, String.format("onSaveInstanceState ()"));

        String url = "http://67.209.116.246:3004";
        savedInstanceState.putString("url", url);
        Log.w(TAG, String.format("onSaveInstanceState (), saved url %s", url));

        // --- webview.saveState(bundle) exists, but we haven't been able to
        // make it do anything useful.
        // --- it *appears* to only save the WebBackForwardList.
        // and, when resume happens, the webview isn't ready yet, so
        // restoring is hard.

        super.onSaveInstanceState(savedInstanceState);
    }

    // --- only called if there is saved state.
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.w(TAG, String.format("onRestoreInstanceState ()"));
        String url = savedInstanceState.getString("url", "NONE");
        Log.w(TAG, String.format("onRestoreInstanceState (), saved is %s", url));
        WebView webview = getWebView();
        if (webview == null) {
            Log.w(TAG, String.format("onRestoreInstanceState (), no webview"));
            return;
        }
        Log.w(TAG, String.format("onRestoreInstanceState (), got webview! loading url"));
        webview.loadUrl(url);
    }

    @Override
    protected void onStop() {
        Log.w(TAG, String.format("onStop ()"));
        super.onStop();
    }

    private static boolean _init() {
        configuration = instance.getResources().getConfiguration();

        webview = getWebView();
        if (webview == null) return errorf("init: can't get webview");

        // ------ needs to be on UI thread
        // WebViewClient webviewClient = webview.getWebViewClient();
        // if (webviewClient == null) {
            // Log.w(TAG, String.format("init(): no webview client"));
        // }
        // else {
            // Log.w(TAG, String.format("init(): got webview client"));
        // }

        QtWebViewCommon.scheduleUiNow(instance, new Runnable() {
            public void run () {
                // --- QT provides a webChromeClient.
                // --- setting one here will override important stuff, so
                // not possible to override methods.
                WebChromeClient webChromeClient = webview.getWebChromeClient();
            }
        });

        // // --- ok, this works.
        Intent startIntent = instance.getIntent();
        if (startIntent == null) {
            Log.w(TAG, String.format("init(): no intent started us (?)"));
        }
        else {
            Log.w(TAG, String.format("init(): got start intent"));
            String action = startIntent.getAction();
            String url = startIntent.getDataString();
            Log.w(TAG, String.format("init(): intent was started with action %s and url %s", action, url));
        }

        initJSInterface(webview);

        createNotificationChannel();
        builder = initNotificationBuilder();
        return true;
    }

    // public static boolean init() {
    public static InitDTO init() {
        Log.w(TAG, String.format("Beginning init"));

        boolean _initOk = _init();
        if (!_initOk) return new InitDTO() {
            boolean ok = false;
            boolean hasSaved = false;
            String passphrase = null;
            int roundIdx = -1;
            int numWords = -1;
            String selectedList = null;
        };

        SharedPreferences pref = instance.getPreferences(Context.MODE_PRIVATE);
        String passphrase = pref.getString(PREF_KEY_PASSPHRASE, null);
        Log.w(TAG, String.format("init(): got stored passphrase: %s", passphrase));
        int roundIdx = pref.getInt(PREF_KEY_ROUND_IDX, -1);
        Log.w(TAG, String.format("init(): got round idx: %d", roundIdx));
        boolean hasSaved = passphrase != null && roundIdx != -1;
        Log.w(TAG, String.format("init(): hasSaved: %b", hasSaved));
        int numWords = pref.getInt(PREF_KEY_NUM_WORDS, -1);
        Log.w(TAG, String.format("init(): got num words: %d", numWords));
        String selectedList = pref.getString(PREF_KEY_SELECTED_LIST, null);
        Log.w(TAG, String.format("init(): got selectedList: %s", selectedList));

        Log.w(TAG, String.format("init successful"));
        initted = true;

        InitDTO ret;
        {
            final boolean h = hasSaved;
            final String p = passphrase;
            final int i = roundIdx;
            final int nw = numWords;
            final String sl = selectedList;

            ret = new InitDTO() {
                boolean ok = true;
                boolean hasSaved = h;
                String passphrase = p;
                int roundIdx = i;
                int numWords = nw;
                String selectedList = sl;
            };
        }

        return ret;
    }

    // --- you can't send the message to QtActivity or QtActivityDelegate
    // (nothing happens).
    // --- if you send it to JNIActivity while it's paused in the
    // background, it hangs or crashes.
    // --- onRestoreInstanceState is not reliable -- seems to often not be
    // called, even when there is state.
    // --- the crashing on 'super onConfigurationChanged not called' is some
    // weird intermittent timing thing: possibly fixed by Qt5.11.1 upgrade.
    // --- we are doing it without setting up a task history list (not
    // necessary).

    private static NotificationCompat.Builder initNotificationBuilder() {
        // --- our app has one activity -- that's what's in the manifest
        // says and that's what reflection on the package manager says.
        // --- but qt is doing stuff behind the scenes, e.g. with a
        // QtActivityDelegate.
        // --- this might be the reason that we can't seem to do stuff like
        // launch the app through an intent if it is already running.

        PackageManager pm = instance.getPackageManager();
        String pack = "cc.alleycat.android.passphrases";
        try {
            PackageInfo pi = pm.getPackageInfo(pack, PackageManager.GET_ACTIVITIES);
            ActivityInfo[] ai = pi.activities;
            Log.w(TAG, String.format("package %s has %d activities", pack, ai.length));
        }
        catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, String.format("couldn't look up package %s", pack));
        }

        // --- IF we kill the activity when notify is called, then this works
        // (brings a fresh activity up again).
		Intent intent = new Intent(instance, JNIActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // --- causes app to crash.
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // --- works -- can be used to send data and pick it up on init.
        // intent.setAction(Intent.ACTION_VIEW);
        // intent.setData(Uri.parse("http://67.209.116.246:3004"));

        PendingIntent pendingIntent = PendingIntent.getActivity(instance, 0, intent, 0);

		return new NotificationCompat.Builder(instance, CHANNEL_ID)
			.setSmallIcon(R.drawable.icon)
			.setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            ;
    }

    private static void createNotificationChannel() {
        // --- api 26+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "some name";
            String description = "some descroopt";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            // --- can't change the importance or other notification behaviors after this.
            NotificationManager notificationManager = (NotificationManager)instance.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(channel);
        }
    }

    public static boolean notify(int id, String shortMsg, String longMsg) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(instance);
        builder
            .setContentTitle(shortMsg)
            .setContentText(longMsg)
            ;
        notificationManager.notify(id, builder.build());
        return true;
    }

    public static boolean pageLoaded() {
        if (!initted) return errorf("pageLoaded: not initted");
        QtWebViewCommon.pageLoaded(instance, webview);
        return true;
    }

    private static WebView getWebView() {
        View child;
        int childCount;
        View rootview = instance.findViewById(android.R.id.content);
        Log.w(TAG, String.format("root: %s", rootview.toString()));

        try {
            FrameLayout rootlayout = (FrameLayout)rootview;
            Log.w(TAG, String.format("root framelayout: %s", rootlayout.toString()));
            childCount = rootlayout.getChildCount();
            Log.w(TAG, String.format("num children of frameview: %d", childCount));
            child = rootlayout.getChildAt(0);
            Log.w(TAG, String.format("child frameview: %s", child.toString()));
            QtLayout qtlayout = (QtLayout)child;
            Log.w(TAG, String.format("qtlayout: %s", qtlayout.toString()));
            childCount = qtlayout.getChildCount();
            Log.w(TAG, String.format("num children of qtlayout: %d", childCount));

            child = qtlayout.getChildAt(0);
            Log.w(TAG, String.format("qtlayout child 0: %s", child.toString()));
            QtSurface qtsurface = (QtSurface)child;

            // --- note, don't try to get children of qtsurface: it's a view.
            Log.w(TAG, String.format("qtsurface: %s", qtsurface.toString()));

            child = qtlayout.getChildAt(1);
            Log.w(TAG, String.format("qtlayout child 1: %s", child.toString()));
        } catch (RuntimeException e) {
            Log.w(TAG, String.format("layout not as expected, can't find webview"));
            return null;
        }

        return (WebView)child;
    }

    private static void storePreferenceString(SharedPreferences.Editor editor, String key, String value) {
        editor.putString(key, value);
        // --- sync to memory, async to disk.
        editor.apply();
        // --- sync write to disk; avoid in main thread.
        // editor.commit();
    }

    private static void storePreferenceInt(SharedPreferences.Editor editor, String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    private static SharedPreferences.Editor getPreferencesEditor() {
        SharedPreferences pref = instance.getPreferences(Context.MODE_PRIVATE);
        return pref.edit();
    }

    public static void storePreference(String key, String value) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        storePreferenceString(editor, key, value);
    }

    public static void storePreference(String key, int value) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        storePreferenceInt(editor, key, value);
    }

    private static void initJSInterface(final WebView webview) {
        instance.runOnUiThread(new Runnable() {
            public void run() {
                webview.addJavascriptInterface(new JsInterface(), HOST_CHANNEL_FROM_JS);
            }
        });
    }

    private static boolean errorf(String err) {
        error(err);
        return false;
    }

    private static void errorv(String err) {
        error(err);
    }

    private static void error(String err) {
        Log.e(TAG, String.format("error: %s", err));
    }

    static class JsInterface {
        @JavascriptInterface
        public void notify(int id, String shortMsg, String longMsg) {
            instance.notify(id, shortMsg, longMsg);
            finish();
        }
        @JavascriptInterface
        public void notifyIfInBackground(int id, String shortMsg, String longMsg) {
            if (resumed) return;
            instance.notify(id, shortMsg, longMsg);
            finish();
        }
        @JavascriptInterface
        public void openLink(String url) {
            instance.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
        @JavascriptInterface
        public void finish() {
            instance.finish();

            // --- if the activity is paused, ensure that it really dies.
            System.exit (0);
            android.os.Process.killProcess(android.os.Process.myPid());
        }
        @JavascriptInterface
        public void storePassphrase(String passphrase) {
            instance.storePreference(PREF_KEY_PASSPHRASE, passphrase);
        }
        @JavascriptInterface
        public void storeRoundIdx(int idx) {
            instance.storePreference(PREF_KEY_ROUND_IDX, idx);
        }
        @JavascriptInterface
        public void storeNumWords(int numWords) {
            instance.storePreference(PREF_KEY_NUM_WORDS, numWords);
        }
        @JavascriptInterface
        public void storeSelectedList(String list) {
            instance.storePreference(PREF_KEY_SELECTED_LIST, list);
        }
    }
}
