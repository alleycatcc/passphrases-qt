import QtQuick 2.10

Rectangle {
  function onKeyPressed (event, webview) {
  }
  function onBackPressed (event, webview) {
  }
  function onKeyReleased (event, webview) {
  }
  function onWebViewLoadStarted (loadRequest, webview) {
  }
  function onWebViewLoadCancelled (loadRequest, webview) {
  }
  function onWebViewLoadFailed (errorCode, errorString, loadRequest, webview) {
  }
  function onWebViewLoadSucceeded (loadRequest, webview) {
  }
  function onWebViewLoadUnknown (loadRequest, webview) {
  }
  function onWebViewComponentCompleted () {
  }
  function onWebViewMaximized () {
    console.log ('webview maximized')
    // --- we're doing this twice in this app, because the first one doesn't
    // zoom out; seems to work.
    pluginRoot.pageLoaded ()
  }
}
