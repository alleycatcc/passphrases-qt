set -x

# --- the entire webroot is in the resources/frontend dir.
# --- in the legacy frontend the files use a relative path (./) and it
# works.
# --- when using a webpack build:
#
#   const RELATIVE_PATH = true
#   output.publicPath = { ...
#     publicPath: RELATIVE_PATH ? './' : '',
#   }

if [ "$catqt_build_target" = android ]; then
  catqt_plugin_webviewlocation=file:///android_asset/frontend/index.html
elif [ "$catqt_build_target" = desktop ]; then
  catqt_plugin_webviewlocation=qrc:/resources/frontend/index.html
else
  error "Not implemented ($catqt_build_target)"
fi

catqt_plugin_androidmainactivityiconlabel=Passphrases

set +x
