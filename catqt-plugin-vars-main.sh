set -eu
set -o pipefail
set -x

# --- provide this file to the build script with -c
# --- all keys must be present.

# --- iconLabel and catqt_plugin_webviewlocation must be overridden by the -variant
# scripts.
# --- all other values can be optionally overridden.

catqt_plugin_windowtitle=Passphrases

# --- no hyphens: becomes the .so name too.
catqt_plugin_projectname=passphrases

catqt_plugin_theme=cat-spinner
catqt_plugin_themever=2

catqt_plugin_qmltype=simple
catqt_plugin_qmlver=2

# ------ android manifest
canonical_packagename=cc.alleycat.android.passphrases

# --- this one is for identifying unique apps on the device.

# it is also used for resolving `R`, so it seems it does have to match the
# actual package.
#
# there is probably a way to have both a test and a release variant on the
# device though.

catqt_plugin_manifestpackagename="$canonical_packagename"
catqt_plugin_mainactivity_packagename="$canonical_packagename".JNIActivity

# --- override in -variant
catqt_plugin_androidmainactivityiconlabel=stub
catqt_plugin_webviewlocation=stub

# --- override in -local-onmain
catqt_plugin_qtdir=stub

catqt_plugin_projqmldir="$catqt_build_projdir"/qml
catqt_plugin_projresourcesdir="$catqt_build_projdir"/resources

projsrcdir="$catqt_build_projdir"/src

catqt_plugin_qmakefilepluginpri="$catqt_build_projdir"/qmake/foreign.pri

catqt_plugin_srcfilesplugin=(
    "$projsrcdir"/catqt_foreign.h
    "$projsrcdir"/cat_pp_root.cpp
    "$projsrcdir"/cat_pp_root.h
)

catqt_plugin_androidsrc="$projsrcdir"/android-sources
catqt_plugin_androidapiversion=28
# --- why is this 16 and the ndk root 10e?
catqt_plugin_androidndkplatform=android-16
catqt_plugin_androidndktoolchainversion=4.9
catqt_plugin_androidarch=armeabi-v7a

set +x
